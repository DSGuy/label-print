// Pulled from Label-Print Dublin that was written in php 9/3/2019

var count = 0;

function presets()
{

}
  
function addlist() 
{
    var list = document.getElementById("AddtoList").innerHTML;
    var hidden = document.getElementById("AddtoHidden").innerHTML;
    var sku =  document.getElementById("sku").value;
        
    if(sku.length > 5) 
    {
        count = count + 1;
        var copys =  document.getElementById("copys").value;
          
       if(copys > 200) 
       {
            copys = 200;

       } else if(copys < 1) 
       {
            copys = 1;
       }

        document.getElementById("AddtoList").innerHTML =list+'<li id="listele'+count+'" class="oddeven list-group-item d-flex justify-content-between align-items-center moblieinput"><button type="button" onclick="deselect('+count+')" style="color:red;"><b>X</b></button>  '+sku+'- Null Description<input type="number" id="thisis'+count+'" onkeyup="editcopys('+sku+','+count+')" max="200" class="form-control form-inline tightnum" placeholder="Qty." value='+copys+'></li>';
        document.getElementById("AddtoHidden").innerHTML =hidden + "<input type='hidden' id='print"+count+"' name='print"+count+"' value='"+sku+"/"+copys+"'>";
        //hidden+'<input type="hidden" value="'+sku+'/'+copys+'">';
        document.getElementById("printcount").value =count;
        
    }
    document.getElementById("sku").value = null;
    document.getElementById("copys").value = null;
        
}

function deselect(thisis) 
{
    var tempt = document.getElementById("listele"+thisis);
    var hiddentmpt = document.getElementById("print"+thisis);
    tempt.parentNode.removeChild(tempt);
    hiddentmpt.parentNode.removeChild(hiddentmpt);
}
    
function select(number,dropnum) 
{

    //this code allows drop down text box to change value for user
    //number is item number on list 
    //dropnum is the numbered dropdown menu added

    //coded like this so i dont need to repeat my code over and over for every drop down menu

    var text = document.getElementById(number + "/" + dropnum).innerHTML; 
    var value = document.getElementById(number + "/" + dropnum).getAttribute("value"); 



    document.getElementById("drop" + dropnum).innerHTML = text;
    document.getElementById("info" + dropnum).value = value;

    document.getElementById("drop" + dropnum).style.background = "green";

}
          
function editcopys(sku, copynum,desc)
{
    var tempnum  = document.getElementById("thisis" + copynum).value;
    document.getElementById("print" + copynum).value = sku + "/" + tempnum + "/" + desc;
}
          
function showprint(bta)
{
    if(bta === "ARN")
    {
        document.getElementById("ArnPrinters").style.display = "block";
        document.getElementById("GraftonPrinters").style.display = "none";
        document.getElementById("CorkPrinters").style.display = "none";
        document.getElementById("LimerickPrinters").style.display = "none";
        document.getElementById("GalwayPrinters").style.display = "none";
        document.getElementById("DHLPrinters").style.display = "none";
        document.getElementById("DundrumPrinters").style.display = "none";
        document.getElementById("BTlabels").style.display = "none";
        document.getElementById("BtPOs").style.display = "none";
        document.getElementById("DHLPOs").style.display = "none";
        document.getElementById("BtPCs").style.display = "none";
          
          
    }

    else if(bta === "CORK")
    {
        document.getElementById("ArnPrinters").style.display = "none";
        document.getElementById("GraftonPrinters").style.display = "none";
        document.getElementById("CorkPrinters").style.display = "block";
        document.getElementById("LimerickPrinters").style.display = "none";
        document.getElementById("GalwayPrinters").style.display = "none";
        document.getElementById("DHLPrinters").style.display = "none";
        document.getElementById("DundrumPrinters").style.display = "none";
        document.getElementById("DHLPOs").style.display = "none";
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
        document.getElementById("BtPCs").style.display = "block";
    }

    else if(bta === "LIME")
    {
        document.getElementById("ArnPrinters").style.display = "none";
        document.getElementById("GraftonPrinters").style.display = "none";
        document.getElementById("CorkPrinters").style.display = "none";
        document.getElementById("LimerickPrinters").style.display = "block";
        document.getElementById("GalwayPrinters").style.display = "none";
        document.getElementById("DHLPrinters").style.display = "none";
        document.getElementById("DundrumPrinters").style.display = "none";
        document.getElementById("DHLPOs").style.display = "none";
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
        document.getElementById("BtPCs").style.display = "block";
    }
        
    else if(bta === "GAL")
    {
        document.getElementById("ArnPrinters").style.display = "none";
        document.getElementById("GraftonPrinters").style.display = "none";
        document.getElementById("CorkPrinters").style.display = "none";
        document.getElementById("LimerickPrinters").style.display = "none";
        document.getElementById("GalwayPrinters").style.display = "block";
        document.getElementById("DHLPrinters").style.display = "none";
        document.getElementById("DundrumPrinters").style.display = "none";
        document.getElementById("DHLPOs").style.display = "none";
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
        document.getElementById("BtPCs").style.display = "block";
    }

    else if(bta === "DHL")
    {
        document.getElementById("ArnPrinters").style.display = "none";
        document.getElementById("GraftonPrinters").style.display = "none";
        document.getElementById("CorkPrinters").style.display = "none";
        document.getElementById("LimerickPrinters").style.display = "none";
        document.getElementById("GalwayPrinters").style.display = "none";
        document.getElementById("DHLPrinters").style.display = "block";
        document.getElementById("DundrumPrinters").style.display = "none";
        document.getElementById("DHLPOs").style.display = "block";
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
        document.getElementById("BtPCs").style.display = "none";
    }

    else if(bta === "DUN") 
    {
        document.getElementById("ArnPrinters").style.display = "none";
        document.getElementById("GraftonPrinters").style.display = "none";
        document.getElementById("CorkPrinters").style.display = "none";
        document.getElementById("LimerickPrinters").style.display = "none";
        document.getElementById("GalwayPrinters").style.display = "none";
        document.getElementById("DHLPrinters").style.display = "none";
        document.getElementById("DundrumPrinters").style.display = "block";
        document.getElementById("DHLPOs").style.display = "none";
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
        document.getElementById("BtPCs").style.display = "block";
    }

    else if(bta === "GRAF") 
    {
        document.getElementById("ArnPrinters").style.display = "none";
        document.getElementById("GraftonPrinters").style.display = "block";
        document.getElementById("CorkPrinters").style.display = "none";
        document.getElementById("LimerickPrinters").style.display = "none";
        document.getElementById("GalwayPrinters").style.display = "none";
        document.getElementById("DHLPrinters").style.display = "none";
        document.getElementById("DundrumPrinters").style.display = "none";
        document.getElementById("DHLPOs").style.display = "none";
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
        document.getElementById("BtPCs").style.display = "block";
    }

    else if(bta === "BTLBL") 
    {
        document.getElementById("BTlabels").style.display = "block";
        document.getElementById("BtPOs").style.display = "block";
    }

    else 
    {
        document.getElementById("BtPOs").style.display = "none";
        document.getElementById("BtPCs").style.display = "none";
        document.getElementById("BTlabels").style.display = "none";
    }         
}
